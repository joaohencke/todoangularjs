angular
  .module('contentools')
  .component('ideasComponent', {
    templateUrl: 'app/idea/component/_idea_component.html',
    /** @ngInject */
    controller($uibModal, $filter, IdeaService) {
      const ctrl = this;

      ctrl.view = {
        isList: true,
        applyFilter() {
          this.items = $filter('filter')(this.allItems, ctrl.criteria);
        },
        detail(item) {
          $uibModal
            .open({
              component: 'ideaDetailComponent',
              resolve: {
                idea() {
                  return item;
                },
                author: () => {
                  const data = this.grouped[item.author.user.email];
                  return Object.assign({}, data.author, {
                    ideas: data.ideas
                  });
                }
              }
            })
            .result
            .then(angular.noop)
            .catch(angular.noop);
        }
      };

      this.$onInit = function () {
        IdeaService.load()
          .then(data => {
            ctrl.view.grouped = data.grouped;
            ctrl.view.allItems = data.items;
            ctrl.view.applyFilter();
          });
      };
    }
  });
