angular.module('contentools')
  .config(IdeaConfig);

/** @ngInject */
function IdeaConfig($stateProvider) {
  $stateProvider
    .state('idea', {
      url: '/',
      component: 'ideasComponent'
    });
}
