angular.module('contentools')
  .filter('ideaStatusFilter', IdeaStatusFilter);

function IdeaStatusFilter() {
  return function (text, isClass = false) {
    switch (text) {
      case 'approved':
        return isClass ? 'badge-success' : 'Aprovada';
      case 'paused':
        return isClass ? 'badge-warning' : 'Pausada';
      case 'recused':
        return isClass ? 'badge-danger' : 'Recusada';
      default:
        return isClass ? 'badge-secondary' : '---';

    }
  };
}
