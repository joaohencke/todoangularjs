angular
  .module('contentools')
  .service('IdeaService', IdeaService);

/** @ngInject */
function IdeaService($http, $q) {
  this.load = function () {
    return $http.get('app/idea/resources/ideas_suggested.json')
      .then(res => {
        if (!res || !res.status || res.status < 200 || res.status > 300) {
          return $q.reject();
        }
        const data = {};
        const items = res.data.objects;
        const filtered = [];
        for (let i = 0; i < items.length; i++) {
          const item = items[i];

          if (angular.isUndefined(item.author.user.isActive) || !item.author.user.isActive) {
            continue;
          }

          if (angular.isUndefined(data[item.author.user.email])) {
            data[item.author.user.email] = {
              author: item.author.user,
              ideas: []
            };
          }
          item.id = Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, 8);
          filtered.push(item);
          data[item.author.user.email].ideas.push({
            title: item.title,
            status: item.status,
            id: item.id
          });
        }
        return {
          items: filtered,
          grouped: data
        };
      });
  };
}
