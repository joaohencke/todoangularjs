/* eslint-disable */
angular.module('contentools')
  .controller('TodoController', TodoController);
/** @ngInject */
function TodoController($scope, TodoService) {
  $scope.view = {
    done: [],
    undone: [],
    init: () => {

    },
    add(todoForm) {
      TodoService.add({
        id: Date.now(),
        done: false,
        data: this.data
      });
      this.data = '';
      this.reload();
    },
    remove(id) {
      TodoService.remove(id);
      this.reload();
    },
    toggle(todo) {
      TodoService.toggle(todo.id);
      this.reload();
    },
    reload() {
      this.done.length = 0;
      this.undone.length = 0;
      this.done.push.apply(this.done, TodoService.getDone());
      this.undone.push.apply(this.undone, TodoService.getUndone());
    }
  };

  $scope.view.reload();
}
