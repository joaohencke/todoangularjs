angular
  .module('contentools')
  .service('TodoService', TodoService);

function TodoService($filter, Storage) {
  const TodoStorage = new Storage('TodoList', {
    persistent: true
  });

  function getAll(done) {
    const all = this.list();

    return $filter('filter')(all, {
      done
    });
  }

  this.add = TodoStorage.insert.bind(TodoStorage);

  this.remove = TodoStorage.remove.bind(TodoStorage);

  this.list = TodoStorage.list.bind(TodoStorage);

  this.get = TodoStorage.get.bind(TodoStorage);

  this.toggle = function (id) {
    const data = this.get(id);
    data.done = !data.done;
    this.add(data);
  };

  this.getDone = function () {
    return getAll.bind(this)(true);
  };

  this.getUndone = function () {
    return getAll.bind(this)(false);
  };
}
