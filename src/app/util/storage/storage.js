angular
  .module('contentools')
  .factory('Storage', StorageService);

function StorageService($localStorage) {
  const has = Object.prototype.hasOwnProperty;

  function Storage(name, opts) {
    this.name = name;
    this.data = [];
    this.indexes = {};
    for (const key in opts) {
      if (has.call(opts, key)) {
        this[key] = opts[key];
      }
    }

    if (opts.persistent) {
      if ($localStorage[name]) {
        this.data = $localStorage[name].data;
        this.indexes = $localStorage[name].indexes;
      }

      $localStorage[name] = this;
    }
  }

  Storage.prototype.insertAll = function (items = []) {
    for (let i = 0; i < items.length; i++) {
      this.insert(items[i]);
    }
  };

  Storage.prototype.insert = function (item) {
    if (angular.isDefined(this.indexes[item.id])) {
      this.data[this.indexes[item.id]] = item;
      return;
    }

    this.data.push(item);
    this.indexes[item.id] = this.data.length - 1;
  };

  Storage.prototype.get = function (id) {
    return this.data[this.indexes[id]];
  };

  Storage.prototype.remove = function (id) {
    if (angular.isDefined(this.indexes[id])) {
      this.data.splice(this.indexes[id], 1);
      this.indexer();
    }
  };

  Storage.prototype.indexer = function () {
    this.indexes = {};

    for (let i = 0, l = this.data.length; i < l; i++) {
      const data = this.data[i];
      this.indexes[data.id] = i;
    }
  };

  Storage.prototype.list = function () {
    return angular.copy(this.data);
  };
  return Storage;
}
