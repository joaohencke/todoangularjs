angular
  .module('contentools', ['ui.router', 'ngStorage', 'ui.bootstrap'])
  .config(config);

/** @ngInject */
function config($stateProvider, $urlRouterProvider, $locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
  $urlRouterProvider.otherwise('/');
}
